# Get Started
<br />

## function takes raw css and return MUI-sx style css

```
import {ParseCSStoMUI} from "css-to-mui-sx"
```
<br />

## Example

### Raw css
![raw css](/lib/raw_css.png)
<br />
<br />
<br />

turns into
<br />
<br />
<br />
### MUI style
![mui](/lib/mui-style.png)
<br />

## import the regex for self-customize css format
```
import {selectorREGEX, declarationsREGEX,propertiesREGEX,valuesREGEX} from "css-to-mui-sx"
```
