export const selectorREGEX = new RegExp(
  "[a-zA-Z0-9.,>:#\\-\n\\s]+(?={)|[a-z.,\\-\n\\s]+(?={)",
  "g"
);
export const declarationsREGEX = new RegExp(
  '[a-zA-Z0-9.)("\\/,%;:!#\\-\n\\s]+(?=})',
  "g"
);
export const propertiesREGEX = new RegExp("[\\s]+[a-z-]+(?=:)", "g");
export const valuesREGEX = new RegExp(`:(.*?)+(?=;)`, "g");

const exampleCSS_string = `
  table tbody,
  table tr,
  table td,
  table p {
    padding: 0;
    margin: 0;
  }
  #logo{
    height:100%;
  }
  .email-template {
    background-color: #fafafa;
  }
  .email-template p {
    padding: 0;
    margin: 0;
  }
  .email-template .table-main {
    margin: 100px 0 50px;
    width: 600px;
  }
  .email-template .brand-banner>img {
    width: 120px;
  }
  .email-template .header-banner {
    background-image: url("https://xxxxxxxxx.png");
    background-size: cover;
  }
  .email-template .email-content button:hover {
    background-color: white;
  }
  .email-template .email-content button:hover {
    background-color: white;
  }
`;
function GetSelectors(cssString) {
  return cssString.match(selectorREGEX);
}

function GetDeclarations(cssString) {
  return cssString.match(declarationsREGEX);
}
function FirstUpperCase(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
function ToCamelCase(string) {
  if (string.includes("-")) {
    const splitString = string.split("-");
    splitString[1] = FirstUpperCase(splitString[1]);
    return splitString.join("");
  }
  return string;
}
function RemoveSpace(string) {
  return string.replaceAll(/\n|\r/g, "").trim();
}
function FormatSelector(selector, index) {
  if (index != 0) {
    return RemoveSpace(selector)
      .split(", ")
      .map((each) => `& ${each}`)
      .join(",");
  }
  return RemoveSpace(selector);
}
export function ParseCSStoMUI(cssString) {
  const muiStyle = {};
  //get all selectors, type : array
  const selectors = GetSelectors(cssString).map((selector, index) =>
    FormatSelector(selector, index)
  );
  // get all declarations of each selector as a single string, type : array
  const declarations = GetDeclarations(cssString);
  // //map the declarations array to modify each property and value
  declarations.map((declaration, index) => {
    const propertiesList = declaration
      .match(propertiesREGEX)
      .map((property) => ToCamelCase(property).trim());
    const valuesList = declaration
      .match(valuesREGEX)
      .map((value) => value.slice(1, value.length).trim());
    const declarationObj = propertiesList.reduce((newObj, property, index) => {
      newObj[property] = valuesList[index];
      return newObj;
    }, {});
    muiStyle[selectors[index]] = declarationObj;
  });
  return muiStyle;
}
ParseCSStoMUI(exampleCSS_string);
